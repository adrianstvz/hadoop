#!/bin/bash

echo -e "Start HDFS\n"
${HADOOP_HOME}/sbin/start-dfs.sh

echo -e "Start YARN\n"
${HADOOP_HOME}/sbin/start-yarn.sh

echo -e "Start HBASE\n"
${HBASE_HOME}/bin/start-hbase.sh
