from pyspark.sql import SparkSession

sparkSession = SparkSession.builder.master('yarn').appName('battle_analysis').getOrCreate()

battles = sparkSession.read.csv('hdfs:///data/battles/battles.csv', header="true")
actors = sparkSession.read.csv('hdfs:///data/battles/battle_actors.csv', header="true")

battles.cache()
actors.cache()


print("Numero total de batallas: ", battles.count())

print("Guerras con mas batallas: ")
battles.groupBy("war").count().orderBy('count', ascending=False).show(10)

print("Numero total de estados con guerras: ", battles.count())

print("Estados con mas batallas: ")
actors.groupBy("actor").count().orderBy('count', ascending=False).show(10)


attackers = actors.filter("attacker == 1").selectExpr("isqno as id","actor as attacker")
defenders = actors.filter("attacker == 0").selectExpr("isqno as id","actor as defender")

print("Diez paises con mas enfrentamientos")
attackers.join(defenders, "id" ).groupby("attacker","defender").count().orderBy("count",ascending=False).show(10)

