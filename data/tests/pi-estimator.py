from pyspark import SparkConf, SparkContext

conf = SparkConf().setMaster('yarn').setAppName('pi-estimator')
sc = SparkContext.getOrCreate(conf=conf)

NUM_SAMPLES=10000

def inside(p):
    import random
    x, y = random.random(), random.random()
    return x*x + y*y < 1


count = sc.parallelize(range(0, NUM_SAMPLES)) \
             .filter(inside).count()
print (" - Pi is roughly %f" % (4.0 * int(count) / NUM_SAMPLES))
