from pyspark import SparkConf, SparkContext

conf = SparkConf().setMaster('yarn').setAppName('pyspark-test')
sc = SparkContext.getOrCreate(conf=conf)

def mod(x):
    import numpy as np
    return (x, np.mod(x, 2))

rdd = sc.parallelize(range(1000)).map(mod).take(10)
print(rdd)

