#!/bin/bash

rm /tmp/*.pid

sudo service ssh start

# Start HDFS and YARN
$HADOOP_HOME/sbin/start-dfs.sh
$HADOOP_HOME/sbin/start-yarn.sh

echo -e "\n## HADOOP STARTED\n"

# Start HBASE
$HBASE_HOME/bin/start-hbase.sh

echo -e "\n## HBASE STARTED\n"

# Create spark-logs directory
$HADOOP_HOME/bin/hdfs dfs -mkdir /spark-logs

# Start history server
$SPARK_HOME/sbin/start-history-server.sh

echo -e "\n## SPARK STARTED\n"

# Create DATA dir inside HDFS
$HADOOP_HOME/bin/hdfs dfs -mkdir /data

# Add battles csv to HDFS dir
$HADOOP_HOME/bin/hdfs dfs -put /data/battles/ /data


# Show status and start bash
jps

# Start jupyter through pyspark
echo "Starting Jupyter..."

export JUPYTER_OPTS="notebook --no-browser --notebook-dir=/data/notebooks --NotebookApp.token='' --port=8080 --ip=0.0.0.0"
export PYSPARK_DRIVER_PYTHON="jupyter"
export PYSPARK_DRIVER_PYTHON_OPTS="$JUPYTER_OPTS"

echo $PYSPARK_DRIVER_PYTHON_OPTS
#jupyter notebook --no-browser --notebook-dir=/data/notebooks --NotebookApp.token='' --port=8080 --ip=0.0.0.0


pyspark --name "Jupyter Spark"



