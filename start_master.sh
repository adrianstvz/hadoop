#!/bin/bash

_name="hadoop-master"
_ip="192.168.1.100"
_network="${1}"
_image="${2}"

docker run -dit \
	--restart=unless-stopped \
	--name ${_name} \
	--hostname ${_name} \
	--net=${_network} \
	-p 2181:2181 \
	-p 8080:8080 \
	-p 8088:8088 \
	-p 9870:9870 \
	-p 16010:16010 \
	-p 18080:18080 \
	-v $(pwd)/data:/data \
	--cpus="2" \
	--memory="4g" \
	${_image}
