## Cluster de Hadoop en Docker



Para ejecutar este cluster es necesario descargar el proyecto:

`git clone https://gitlab.com/adrianstvz/hadoop`


Una vez descargado, se puede utilizar el Makefile incluido para facilitar la manipulación del cluster.

- make build: crea las imágenes de Docker
- make run: crea las imágenes de Docker y despliega el cluster
- make start, stop, restart: permiten manipular el cluster

Se recomienda un mínimo de 4GB de memoria libres

Una vez iniciado el cluster se puede consultar el estado de los contenedores con:

`docker logs "container name" -f`


Y entrar en ellos con:

`docker exec -it "container name" bash`


Además, las siguientes interfaces web son accesibles:

- HBASE:http://localhost:16010/master-status
- HDFS Namenode:http://localhost:9870
- History Server:http://localhost:18080
- Jupyter Notebooks:http://localhost:8080
- YARN:http://localhost:8088/cluster/apps

