#!/bin/bash

_name="hadoop-slave-${1:-0}"
_ip="192.168.1.10${1:-1}"
_network="${2}"
_image="${3}"

docker run -dit \
	--restart=unless-stopped \
	--name ${_name} \
	--hostname ${_name} \
	--net=${_network} \
	--cpus="1" \
	--memory="2g" \
	${_image}
