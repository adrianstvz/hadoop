#!/bin/bash

.PHONY: help build rebuild clean run prune start stop


help:
	@echo "Usage: make [TARGET]"
	@echo "Targets: "
	@echo -e "\tbuild"
	@echo -e "\tclean"
	@echo -e "\thelp"
	@echo -e "\tprune"
	@echo -e "\trebuild"
	@echo -e "\trun"
	@echo -e "\tstart"
	@echo -e "\tstop"


_base_image="ae/hadoop_base"
_master_image="ae/hadoop_master"
_base_dir="base"
_master_dir="master"

_image="hadoop"

net_driver="bridge"
net_name="hadoop_net"
_name="hadoop-"

#MAKE
all: clean run check

# Build the images
build:
	@docker build -t $(_base_image) $(_base_dir) && \
	docker build -t $(_master_image) $(_master_dir)
	@echo "Images built"

# Rebuild the image
rebuild:
	@docker build --no-cache -t $(_base_image) $(_base_dir) && \
	docker build --no-cache -t $(_master_image) $(_master_dir) 
	@echo "Images rebuilt"

# Clean the container
clean: stop
	@docker network rm $(net_name) || true
	@docker rm `docker ps -aqf name=^$(_name)` &> /dev/null \
		&& echo "Project $(_image) cleaned" \
		|| echo "Nothing to clean"

	
# Run the container
run: build start

start: clean
	# Create Network
	@docker network create \
		--driver=$(net_driver) \
		$(net_name) || true
	# Start slaves
	@for _i in $$(seq 1 2); do \
		./start_slave.sh $${_i} $(net_name) $(_base_image); \
	done
	# Start master
	@./start_master.sh $(net_name) $(_master_image);


# Check the container
check:
	@docker ps -a -f name=^$(_name)$$ |\
		awk '{OFS="\t"; if (FNR == 1) print $$1" "$$2, $$3,"",$$NF; \
						else print $$1, $$2, $$NF}' 

stop:
	@docker stop $$(docker ps -aqf name=^$(_name)) &> /dev/null \
		&& echo "Containers $(_name)* stopped" \
		|| echo "No container to stop"

